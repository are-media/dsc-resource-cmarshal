function Get-TargetResource
{
    [CmdletBinding()]
    [OutputType([System.Collections.Hashtable])]
    param
    (
        [Parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [String]$Name,

        [Parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [String]$MarshalExeFullPath,

        [ValidateSet("Present", "Absent")]
        [String]$Ensure = "Present"
    )

    return @{
        ServiceName = $name
        MarshalExeFullPath = $MarshalExeFullPath
        Ensure = $Ensure
    }
}

function Set-TargetResource
{
    [CmdletBinding()]
    param
    (
        [Parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [String]$Name,

        [Parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [String]$MarshalExeFullPath,

        [ValidateSet("Present", "Absent")]
        [String]$Ensure = "Present"
    )

    if ($Ensure -eq "Present")
    {
        Install-MarshalService -MarshalExeFullPath $MarshalExeFullPath
    }
    elseif ($Ensure -eq "Absent")
    {
        Remove-MarshalService -MarshalExeFullPath $MarshalExeFullPath
    }
}

function Test-TargetResource
{
    [CmdletBinding()]
    [OutputType([System.Boolean])]
    param
    (
        [Parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [String]$Name,

        [Parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [String]$MarshalExeFullPath,

        [ValidateSet("Present", "Absent")]
        [String]$Ensure = "Present"
    )

    return $false
}

function Install-MarshalService
{
    param
    (
        [Parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [String]$MarshalExeFullPath
    )

    Remove-MarshalService -MarshalExeFullPath $MarshalExeFullPath

    Start-Process "$MarshalExeFullPath" `
        -ArgumentList @("--install") `
        -Wait

    # adding sleep because for strange reason marshal is not starting up
    Start-Sleep -Seconds 30

    Start-Process "$MarshalExeFullPath" `
        -ArgumentList @("--start") `
        -Wait

    try
    {
        # adding this sleep to try starting second time because for strange reason marshal is not starting up
        Start-Sleep -Seconds 10

        Start-Process "$MarshalExeFullPath" `
            -ArgumentList @("--start") `
            -Wait
    }
    catch{}
}

function Remove-MarshalService
{
    param
    (
        [Parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [String]$MarshalExeFullPath
    )

    try{
        Start-Process "$MarshalExeFullPath" `
        -ArgumentList @("--uninstall") `
        -Wait
    }
    catch{}
}

#  FUNCTIONS TO BE EXPORTED
Export-ModuleMember -function Get-TargetResource, Set-TargetResource, Test-TargetResource
