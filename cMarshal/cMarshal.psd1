@{
# Version number of this module.
ModuleVersion = '1.0.0.0'

# ID used to uniquely identify this module
GUID = '3798d0f8-9e01-4ce4-8732-99334472f732'

# Author of this module
Author = 'Bauer Media Dev Team'

# Company or vendor of this module
CompanyName = 'Bauer Media Ltd'

# Copyright statement for this module
Copyright = '(c) 2015 Bauer Media Ltd. All rights reserved.'

# Description of the functionality provided by this module
Description = 'Custom DSC Resources for managing NServiceBus related applications'

# Minimum version of the Windows PowerShell engine required by this module
PowerShellVersion = '4.0'

# Minimum version of the common language runtime (CLR) required by this module
CLRVersion = '4.0'

# Functions to export from this module
FunctionsToExport = '*'

# Cmdlets to export from this module
CmdletsToExport = '*'
}
