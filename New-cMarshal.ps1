﻿$moduleName = "cMarshal"
$powershellPath = "C:\Program Files\WindowsPowerShell\Modules"
$modulePath = Join-Path $powershellPath $moduleName
$moduleDscPath = Join-Path $modulePath "DSCResources"


#### Setup cNServiceBusHost #####
$resourceFriendlyName = "cMarshalService"
$resourceName = "Bauer_$resourceFriendlyName"
$resoucePath = Join-Path $moduleDscPath $resourceName

$name = New-xDscResourceProperty -Name "Name" -Type String -Attribute Key -Description "Name of the marshal"
$marshalExeFullPath = New-xDscResourceProperty -Name "MarshalExeFullPath" -Type String -Attribute Required -Description "Marshal executable file location (Full path including file name)"
$ensure = New-xDscResourceProperty     -Name "Ensure"     -Type String   -Attribute Write    -Description "Whether to install or uninstall Marshal" -ValidateSet @("Present", "Absent")

$properties = @($name, $marshalExeFullPath, $ensure)

Write-Host "resource path is $resoucePath"

if (Test-Path $resoucePath)
{
    Write-Host "Updating resource $resourceName"
    Update-xDscResource -Name $resourceName -Property $properties -Force
}
else
{
    Write-Host "Creating new resource $resourceName"
    New-xDscResource -Name $resourceName -Property $properties -Path $powershellPath -ModuleName $moduleName -FriendlyName $resourceFriendlyName -Force
}